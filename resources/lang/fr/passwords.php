<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Le mot de passe doit comporter au minimum 6 caractères et correspondre avec le champ de confirmation.',
    'reset' => 'Votre mot de passe a été réinitialisé !',
    'sent' => 'Nous vous avons envoyé un email pour réinitiliser votre mot de passe.',
    'token' => 'Votre code de réinitialisation de mot de passe est incorrect.',
    'user' => "Aucun utilisateur n'a été trouvé pour cette adresse mail.",

];
