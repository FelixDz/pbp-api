<?php

return [

	// Shortcuts
    'albums_shortcode' => false,
    'slug_shortcut'=> false,
    'id_shortcut'=> false,

    // Exif
    'image_exif_data'=> false,

    // Default template
    'default_album_template'=> "default", //default - bootstrap - special

    'template_folder' => [
        'default' => 'albums.default',
        'bootstrap' => 'albums.bootstrap',
        'special' => 'albums.special',
        'bootstrap4' => 'albums.bootstrap4',
    ],

	// Do not use 'image_folder'=>url("/storage")
    'image_folder'=>config('app.url')."/storage",
];

