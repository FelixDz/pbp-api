### PBP-API : _a RESTful API project to be used alongside the brewery's website._

The API is consumed by the website server/client, entrypoints beeing readable by everyone and write permissions being reserved to the admin users.

Using : `Laravel`

***

#### Useful commands

Remember 'em !

##### Update composer dependencies (defined in composer.json) :
```php
composer update
```

##### Generate all useful files from a table definition file (resourceX-generator.yaml) :
_NOTE : do not add an 'id' column in the definition file, one is already automagically created._
>__Warning__ : until the original repo for generators is updated, some additionnal steps are needed after running the command :
> - replace `$app` by `$router` in `routes/web.php`
> - replace `drop` by `dropIfExists` in the `down()` method in all the migration files
> - create a separate factory from the code added to `database/factories/ModelFactory.php`
> - create appropriate seeder in `database/seeds` with `php artisan make:seeder resourceXsTableSeeder` and add a call to it in `DatabaseSeeder.php`

```php
php artisan wn:resources api-generators/resourceX-generator.yaml [--path=...] [--force=true]
```

##### Make a new migration on a specific existing table :
```php
php artisan make:migration migration_name --table=xxxxx
```
Related docs : [https://laravel.com/docs/8.x/migrations](https://laravel.com/docs/8.x/migrations)

##### Reverse all database migrations, re-run them and populate the DB with seeders :
```php
php artisan migrate:refresh --seed
```

##### Dump all database data, run migration and populate the DB with seeders :
```php
php artisan migrate:fresh --seed
php artisan voyager:admin felix@zaclys.net
```

##### After fresh install, run :
```php
php artisan voyager:install
php artisan voyager:admin felix@zaclys.net
```

##### Seeding using only a specific seeder (in prod, to skip the warning use the `--force` flag) :
```php
php artisan db:seed --class=TheSpecificSeeder [--force]
```

***

#### Troubleshooting

##### Vagrant / Virtualbox / Homestead upgrade

See [this Stackoverflow](https://stackoverflow.com/a/64254041)

And [this for Homestead](https://w3guy.com/update-laravel-homestead-vagrant-box/)

##### Composer for dependencies upgrades

Edit the dependency spec in `composer.json` and then run

```bash
composer clear-cache; rm -rf vendor; rm composer.lock
composer install
```

If you got a memory error running `composer install` or `composer require`, use :

```bash
COMPOSER_MEMORY_LIMIT=-1 composer require …
```

#### Maintenance mode !

See [this page](https://laravel.com/docs/8.x/configuration#maintenance-mode)
