<?php

namespace App\Http\Controllers;

use TCG\Voyager\Database\Schema\SchemaManager;
use TCG\Voyager\Events\BreadDataAdded;
use TCG\Voyager\Events\BreadDataDeleted;
use TCG\Voyager\Events\BreadDataUpdated;
use TCG\Voyager\Events\BreadImagesDeleted;
use TCG\Voyager\Http\Controllers\Controller;
use TCG\Voyager\Http\Controllers\Traits\BreadRelationshipParser;
use Voyager;
use VoyagerThemes\Models\Theme;
use VoyagerThemes\Models\ThemeOptions;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Validator;

use App\Models\Album;

class AlbumsController extends Controller {

    const MODEL = "App\Models\Album";

    use RESTActions;
    use BreadRelationshipParser;

    // Special GET ALL : ordering
    public function all()
    {
        $m = self::MODEL;
        return $this->respond(Response::HTTP_OK, $m::all()->where('active', '1')->sortBy('order')->makeHidden(['photos', 'desc', 'active', 'order'])->values());
    }

    // Special GET : hide useless data from response (lighter JSON)
    public function get($id)
    {
        $m = self::MODEL;
        $model = $m::find($id)->makeHidden(['active', 'order']);
        if(is_null($model)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $model);
    }

    public function index(Request $request)
    {
        $slug = "albums";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

		// Check permission
        $this->authorize('browse', app($dataType->model_name));

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'browse');

        // Albums ordering
        $orderBy = $request->get('order_by', $dataType->order_column);
        $sortOrder = $request->get('sort_order', $dataType->order_direction);
        $orderColumn = [];
        if ($orderBy) {
            $index = $dataType->browseRows->where('field', $orderBy)->keys()->first();
            $orderColumn = [[$index, $sortOrder ?? 'desc']];
        }

        $model = app($dataType->model_name);
        if ($dataType->scope && $dataType->scope != '' && method_exists($model, 'scope'.ucfirst($dataType->scope))) {
            $query = $model->{$dataType->scope}();
        } else {
            $query = $model::select('*');
        }

        if ($orderBy && in_array($orderBy, $dataType->fields())) {
            $querySortOrder = (!empty($sortOrder)) ? $sortOrder : 'desc';
            $albums = call_user_func([
                $query->orderBy($orderBy, $querySortOrder),
                'get',
            ]);
        } elseif ($model->timestamps) {
            $albums = call_user_func([$query->latest($model::CREATED_AT), 'get']);
        } else {
            $albums = call_user_func([$query->orderBy($model->getKeyName(), 'DESC'), 'get']);
        }

        // Replace relationships' keys for labels and create READ links if a slug is provided.
        $albums = $this->resolveRelations($albums, $dataType);

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($albums);

        return Voyager::view('voyager::albums.browse', compact(
            'albums',
            'dataType',
            'isModelTranslatable',
            'orderBy',
            'orderColumn',
            'sortOrder'
        ));
    }

    public function create(Request $request)
    {
		// Check permission
        $slug = "albums";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? new $dataType->model_name()
            : false;

        foreach ($dataType->addRows as $key => $row) {
            $details = $row->details;
            $dataType->addRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        $this->authorize('add', app($dataType->model_name));

        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);

        $nextID = $this->getNextGeneratedID();

        return Voyager::view('voyager::albums.edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'nextID'));
    }

    public function store(Request $request)
    {
        // Check permission
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $this->authorize('edit', app($dataType->model_name));

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->addRows, $dataType->model_name);
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        if (!$request->ajax()) {
            $data = $this->insertUpdateData($request, $slug, $dataType->addRows, new $dataType->model_name());

            event(new BreadDataAdded($dataType, $data));

            return redirect()
                ->route("voyager.{$dataType->slug}.index")
                ->with([
                    'message'    => __('albums.message.new_album', ['album' => $request->name]),
                    'alert-type' => 'success',
                ]);
        }
    }

    public function activate_album(Request $request)
    {
        // Check permission
        $slug = "albums";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();
        $this->authorize('edit', app($dataType->model_name));

        $data = Album::whereid($request->id)->firstOrFail();
        $data->active = true;
        $data->save();

        return redirect()->back()->with([
            'message'    => __('albums.message.activate_album',['album' => $data->title]),
            'alert-type' => 'success',
        ]);

    }

    public function desactivate_album(Request $request)
    {
        $slug = "albums";
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $data = Album::whereid($request->id)->firstOrFail();
        $data->active = false;
        $data->save();

        return redirect()
            ->back()
            ->with([
                'message'    => __('albums.message.desactivate_album',['album' => $data->title]),
                'alert-type' => 'success',
            ]);
    }

    public function edit(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;

        $dataTypeContent = (strlen($dataType->model_name) != 0)
            ? app($dataType->model_name)->findOrFail($id)
            : DB::table($dataType->name)->where('id', $id)->first(); // If Model doest exist, get data from table name

        foreach ($dataType->editRows as $key => $row) {
            $details = $row->details;
            $dataType->editRows[$key]['col_width'] = isset($details->width) ? $details->width : 100;
        }

        // If a column has a relationship associated with it, we do not want to show that field
        $this->removeRelationshipField($dataType, 'edit');

        // Check permission
        $this->authorize('edit', $dataTypeContent);
        $album = Album::whereid($id)->firstOrFail();
        // Check if BREAD is Translatable
        $isModelTranslatable = is_bread_translatable($dataTypeContent);
        return Voyager::view('voyager::albums.edit-add', compact('dataType', 'dataTypeContent', 'isModelTranslatable', 'album'));
    }

    public function update(Request $request, $id)
    {
        $slug = $this->getSlug($request);
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Compatibility with Model binding.
        $id = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);

        // Check permission
        $this->authorize('edit', $data);

        // Validate fields with ajax
        $val = $this->validateBread($request->all(), $dataType->editRows, $dataType->model_name, $id);
        if ($val->fails()) {
            return response()->json(['errors' => $val->messages()]);
        }

        $album = Album::whereid($request->id)->firstOrFail();

        if (!$request->ajax()) {
            $this->insertUpdateData($request, $slug, $dataType->editRows, $data);

            event(new BreadDataUpdated($dataType, $data));

            if (@$request->pgs == 1) {
                // fast upload return album image page
                return redirect()
                    ->route("voyager.photos.index",$id)
                    ->with([
                        'message'    => __('albums.message.album_edit',['album'=>$album->name]),
                        'alert-type' => 'success',
                    ]);
            } else {
                //album index redirect
                return redirect()
                    ->route("voyager.{$dataType->slug}.index")
                    ->with([
                        'message'    => __('albums.message.album_edit',['album'=>$album->name]),
                        'alert-type' => 'success',
                    ]);
            }

        }
    }

    public function destroy(Request $request)
    {
        $slug = $this->getSlug($request);
        $id = $request->id;
        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('delete', app($dataType->model_name));

        // Init array of IDs
        $ids = [];
        $album = Album::whereid($request->id)->firstOrFail();
        if (empty($id)) {
            // Bulk delete, get IDs from POST
            $ids = explode(',', $request->ids);
        } else {
            // Single item delete, get ID from URL or Model Binding
            $ids[] = $id instanceof Model ? $id->{$id->getKeyName()} : $id;
        }
        foreach ($ids as $id) {
            $data = call_user_func([$dataType->model_name, 'findOrFail'], $id);
            $this->cleanup($dataType, $data);
        }

        $displayName = count($ids) > 1 ? $dataType->display_name_plural : $dataType->display_name_singular;

        $res = $data->destroy($ids);
        $data = $res
            ? [
                'message'    => __('albums.message.album_delete',['album'=>$album->name]),
                'alert-type' => 'success',
            ]
            : [
                'message'    => __('albums.message.album_delete_error',['album'=>$displayName]),
                'alert-type' => 'error',
            ];

        if ($res) {
            event(new BreadDataDeleted($dataType, $data));
        }

        return redirect()->route("voyager.{$dataType->slug}.index")->with($data);
    }

    protected function cleanup($dataType, $data)
    {
        // Delete Translations, if present
        if (is_bread_translatable($data)) {
            $data->deleteAttributeTranslations($data->getTranslatableAttributes());
        }

        // Delete Images
        $this->deleteBreadImages($data, $dataType->deleteRows->where('type', 'image'));

        // Delete Files
        foreach ($dataType->deleteRows->where('type', 'file') as $row) {
            $files = json_decode($data->{$row->field});
            if ($files) {
                foreach ($files as $file) {
                    $this->deleteFileIfExists($file->download_link);
                }
            }
        }
    }

    public function deleteBreadImages($data, $rows)
    {
        foreach ($rows as $row) {
            $this->deleteFileIfExists($data->{$row->field});

            $options = $row->details;

            if (isset($options->thumbnails)) {
                foreach ($options->thumbnails as $thumbnail) {
                    $ext = explode('.', $data->{$row->field});
                    $extension = '.'.$ext[count($ext) - 1];

                    $path = str_replace($extension, '', $data->{$row->field});

                    $thumb_name = $thumbnail->name;

                    $this->deleteFileIfExists($path.'-'.$thumb_name.$extension);
                }
            }
        }

        if ($rows->count() > 0) {
            event(new BreadImagesDeleted($data, $rows));
        }
    }

    public function order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        if (!isset($dataType->order_column) || !isset($dataType->order_display_column)) {
            return redirect()
            ->route("voyager.{$dataType->slug}.index")
            ->with([
                'message'    => __('voyager::bread.ordering_not_set'),
                'alert-type' => 'error',
            ]);
        }

        $model = app($dataType->model_name);
        if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
            $model = $model->withTrashed();
        }
        $results = $model->orderBy($dataType->order_column, $dataType->order_direction)->get();

        $display_column = $dataType->order_display_column;

        $dataRow = Voyager::model('DataRow')->whereDataTypeId($dataType->id)->whereField($display_column)->first();

        $view = 'voyager::bread.order';

        if (view()->exists("voyager::$slug.order")) {
            $view = "voyager::$slug.order";
        }

        return Voyager::view($view, compact(
            'dataType',
            'display_column',
            'dataRow',
            'results'
        ));
    }

    public function update_order(Request $request)
    {
        $slug = $this->getSlug($request);

        $dataType = Voyager::model('DataType')->where('slug', '=', $slug)->first();

        // Check permission
        $this->authorize('edit', app($dataType->model_name));

        $model = app($dataType->model_name);

        $order = json_decode($request->input('order'));
        $column = $dataType->order_column;
        foreach ($order as $key => $item) {
            if ($model && in_array(SoftDeletes::class, class_uses_recursive($model))) {
                $i = $model->withTrashed()->findOrFail($item->id);
            } else {
                $i = $model->findOrFail($item->id);
            }
            $i->$column = ($key + 1);
            $i->save();
        }
    }

    public function getNextGeneratedID() 
	{
		$statement = DB::select("show table status like 'albums'");
		return $statement[0]->Auto_increment;
	}

    public function validateBread($data, $rows, $name = null, $id = null)
    {
        $rules = [];
        $messages = [];
        $customAttributes = [];
        $is_update = $name && $id;

        $fieldsWithValidationRules = $this->getFieldsWithValidationRules($rows);

        foreach ($fieldsWithValidationRules as $field) {
            $fieldRules = $field->details->validation->rule;
            $fieldName = $field->field;

            // Show the field's display name on the error message
            if (!empty($field->display_name)) {
                if (!empty($data[$fieldName]) && is_array($data[$fieldName])) {
                    foreach ($data[$fieldName] as $index => $element) {
                        if ($element instanceof UploadedFile) {
                            $name = $element->getClientOriginalName();
                        } else {
                            $name = $index + 1;
                        }

                        $customAttributes[$fieldName.'.'.$index] = $field->getTranslatedAttribute('display_name').' '.$name;
                    }
                } else {
                    $customAttributes[$fieldName] = $field->getTranslatedAttribute('display_name');
                }
            }

            // If field is an array apply rules to all array elements
            $fieldName = !empty($data[$fieldName]) && is_array($data[$fieldName]) ? $fieldName.'.*' : $fieldName;

            // Get the rules for the current field whatever the format it is in
            $rules[$fieldName] = is_array($fieldRules) ? $fieldRules : explode('|', $fieldRules);

            if ($id && property_exists($field->details->validation, 'edit')) {
                $action_rules = $field->details->validation->edit->rule;
                $rules[$fieldName] = array_merge($rules[$fieldName], (is_array($action_rules) ? $action_rules : explode('|', $action_rules)));
            } elseif (!$id && property_exists($field->details->validation, 'add')) {
                $action_rules = $field->details->validation->add->rule;
                $rules[$fieldName] = array_merge($rules[$fieldName], (is_array($action_rules) ? $action_rules : explode('|', $action_rules)));
            }
            // Fix Unique validation rule
            foreach ($rules[$fieldName] as &$fieldRule) {
                if (strpos(strtoupper($fieldRule), 'UNIQUE') !== false) {
                    $fieldRule = \Illuminate\Validation\Rule::unique($name)->ignore($id);
                }
            }

            // Set custom validation messages if any
            if (!empty($field->details->validation->messages)) {
                foreach ($field->details->validation->messages as $key => $msg) {
                    $messages["{$field->field}.{$key}"] = $msg;
                }
            }
        }

        return Validator::make($data, $rules, $messages, $customAttributes);
    }
}
