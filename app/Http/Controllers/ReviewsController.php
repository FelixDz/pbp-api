<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ReviewsController extends Controller {

    const MODEL = "App\Models\Review";

    use RESTActions;

	// Special GET ALL : last reviews show up first
    public function all()
    {
        $orderedReviews = DB::table('reviews')->orderBy('created_at', 'desc')->get();
        return $this->respond(Response::HTTP_OK, $orderedReviews);
    }

    // Get only reviews from a specific category
    public function getCategory($category)
    {
        $catReviews = DB::table('reviews')->where('category', '=', $category)->orderBy('created_at', 'desc')->get();
        return $this->respond(Response::HTTP_OK, $catReviews);
    }
}
