<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Response;
use Carbon\Carbon;
// Using Carbon : see http://carbon.nesbot.com/docs/

class NewsItemsController extends Controller {

    const MODEL = "App\Models\NewsItem";

    use RESTActions;

    private $now;
    private $limitInDays;	// Limit used for filtering news near from now (posted less than X days ago or upcoming in less than X days)
    private $maxLatestResults;	// Max number of results returned by the "getLatest" method

    public function __construct()
    {
        Carbon::setLocale('fr');
        $this->now = Carbon::now('Europe/Paris');
        $this->limitInDays = 25;
        $this->maxLatestResults = 6;
    }

    // Override for special ordering
    public function all()
    {
        $news = DB::table('news_items')
        	->where(function ($query) {
        		// Upcoming events (they have not ended)
        		$query
        			->whereIn('category', ['event', 'event_home'])
					->whereDate('ending_date', '>', $this->now->copy()->toDateTimeString());
        	})
        	->orWhere(function ($query) {
        		// Other posts, posted less than X weeks ago
        		$query
        			->whereNotIn('category', ['event', 'event_home']);
        	})
        	->get();

		// Ordering the results by desc post date.
        $newsSorted = $news->sortByDesc('post_date')->values()->all();

        return $this->respond(Response::HTTP_OK, $newsSorted);
    }

	// Latest news with intelligent selection & ordering
    public function getLatest()
    {
        $latestNews = DB::table('news_items')
        	->where(function ($query) {
        		// Upcoming events (they have not ended and start(ed) before X weeks from now)
        		$query
        			->whereIn('category', ['event', 'event_home'])
					->whereDate('ending_date', '>', $this->now->copy()->toDateTimeString())
					->whereDate('starting_date', '<', $this->now->copy()->addDays($this->limitInDays)->toDateTimeString());
        	})
        	->orWhere(function ($query) {
        		// Other posts, posted less than X weeks ago
        		$query
        			->whereNotIn('category', ['event', 'event_home'])
					->whereDate('post_date', '>', $this->now->copy()->subDays($this->limitInDays));
        	})
        	->get();

		// Ordering the results by diff (in days) between the starting date and now.
        // This way, recent posts and upcoming events show up first.
        $latestNewsSorted = $latestNews->sortBy(function ($item) {
			return $this->now->copy()->diffInDays(Carbon::parse($item->starting_date));
        })->values()->all();	// The "->values()->all()" bit is crucial for correct sorting !

        return $this->respond(Response::HTTP_OK, array_slice($latestNewsSorted, 0, $this->maxLatestResults));
    }

	// Get all awards only
    public function allAwards()
    {
        $m = self::MODEL;
        $submodel = $m::all()->where('category', 'award')->sortByDesc('post_date')->values();
        return $this->respond(Response::HTTP_OK, $submodel);
    }

	// Get a specific award
    public function getAward($id)
    {
        $m = self::MODEL;
        $item = $m::all()->where('category', 'award')->where('id', $id)->first();
        if(is_null($item)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $item);
    }

	// Get all events
    public function allEvents()
    {

        $events = DB::table('news_items')
        	->where(function ($query) {
        		// Upcoming events (they have not ended)
        		$query
        			->whereIn('category', ['event', 'event_home'])
					->whereDate('ending_date', '>', $this->now->copy()->toDateTimeString());
        	})
        	->get();

		// Ordering the results by starting date.
        $eventsSorted = $events->sortBy('starting_date')->values()->all();

        return $this->respond(Response::HTTP_OK, $eventsSorted);
    }

    // Get all events take place elsewhere only
    public function allEventsElsewhere()
    {
        $events = DB::table('news_items')
        	->where(function ($query) {
        		// Upcoming events (they have not ended)
        		$query
        			->where('category', 'event')
					->whereDate('ending_date', '>', $this->now->copy()->toDateTimeString());
        	})
        	->get();

		// Ordering the results by starting date.
        $eventsSorted = $events->sortBy('starting_date')->values()->all();

        return $this->respond(Response::HTTP_OK, $eventsSorted);
    }

    // Get all events at the brewery only
    public function allEventsHome()
    {
        $events = DB::table('news_items')
        	->where(function ($query) {
        		// Upcoming events (they have not ended)
        		$query
        			->where('category', 'event_home')
					->whereDate('ending_date', '>', $this->now->copy()->toDateTimeString());
        	})
        	->get();

		// Ordering the results by starting date.
        $eventsSorted = $events->sortBy('starting_date')->values()->all();

        return $this->respond(Response::HTTP_OK, $eventsSorted);
    }

	// Get a specific event
    public function getEvent($id)
    {
        $m = self::MODEL;
        $submodel = $m::all()->whereIn('category', ['event', 'event_home'])->where('id', $id)->first();
        $item = $submodel->find($id);
        if(is_null($item)){
            return $this->respond(Response::HTTP_NOT_FOUND);
        }
        return $this->respond(Response::HTTP_OK, $item);
    }

}
