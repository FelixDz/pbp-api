<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model {

	use HasFactory;

    protected $fillable = ["author", "category", "image", "desc", "via", "rating"];

    protected $dates = [];

    public static $rules = [
        "author" => "required|min:3",
        "category" => "required",
        "image" => "required",
        "desc" => "required",
    ];

    // Relationships

}
