<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Album extends Model {

	use HasFactory;

    protected $fillable = ["title", "slug", "category", "date", "desc", "order", "active", "cover_image"];

    protected $dates = ["date"];
    
    protected $table = "albums";

    public static $rules = [
        "title" => "required",
        "slug" => "required|unique:albums,slug",
        "category" => "required",
        "date" => "date",
        "order" => "numeric",
        "active" => "required",
    ];

    // Relationships

    public function photos()
    {
    	return $this->hasMany('App\Photo', "album_id");
    }

	/**
     * Get the photos for the album.
     *
     * @return Collection
     */

    public function getPhotosAttribute()
    {
        return Photo::all()
        	->where('album_id', $this->id)
        	->transform(function($item){
        		return $item->only(['id', 'image', 'legend', 'sort']);
        	})
        	->sortBy('sort')
        	->values();
    }

	/**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['photos'];

}
