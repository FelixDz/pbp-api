<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model {

	use HasFactory;

    protected $fillable = ["album_id", "legend", "image", "sort" ];

    protected $dates = [];

    public static $rules = [
        "album_id" => "required,numeric",
        "image" => "required",
        "sort" => "numeric",
    ];

    // Relationships

	public function album()
    {
    	return $this->belongsTo('App\Album');
    }
}
