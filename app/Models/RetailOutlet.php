<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RetailOutlet extends Model {

	use HasFactory;

    protected $fillable = ["name", "category", "comment", "address", "zipcode", "city", "phone", "web"];

    protected $dates = [];

    public static $rules = [
        "name" => "required|min:4",
        "category" => "required",
        "address" => "required",
        "zipcode" => "required,numeric",
        "city" => "required",
    ];

    // Relationships

}
