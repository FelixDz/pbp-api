<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model {

	use HasFactory;

    protected $fillable = ["name", "category", "image", "desc", "type", "abv", "body", "bitterness"];

    protected $dates = [];

    public static $rules = [
        "name" => "required",
        "category" => "required",
        "image" => "required",
        "desc" => "required",
        "abv" => "numeric",
        "body" => "numeric",
        "bitterness" => "numeric",
    ];

    // Relationships

    public function awards()
    {
    	return $this->hasMany('App\NewsItem');
    }

	/**
     * Get the awards for the product.
     *
     * @return Collection
     */

    public function getAwardsAttribute()
    {
        return NewsItem::all()
        	->where('category', 'award')
        	->where('product_id', $this->id)
        	->sortByDesc('post_date')
        	->transform(function($item){
        		return $item->only(['id', 'award_title', 'image', 'year']);
        	})
        	->values();
    }

	/**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['awards'];

}
