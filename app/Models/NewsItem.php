<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsItem extends Model {

	use HasFactory;

    protected $fillable = ["title", "category", "image", "desc", "post_date", "starting_date", "ending_date", "hours", "place", "year", "award_title", "product_id"];

    protected $dates = ["post_date", "starting_date", "ending_date"];

    public static $rules = [
        "title" => "required|min:5",
        "category" => "required",
        "desc" => "required",
        "post_date" => "date",
        "starting_date" => "date",
        "ending_date" => "date",
        "year" => "numeric",
    ];

    // Relationships

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

}
