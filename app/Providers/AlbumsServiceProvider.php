<?php

namespace App\Providers;

use TCG\Voyager\Models\Menu;
use TCG\Voyager\Models\MenuItem;
use TCG\Voyager\Models\Permission;
use TCG\Voyager\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Events\Dispatcher;

use App\Models\Album;
use App\Models\Photo;

class AlbumsServiceProvider extends ServiceProvider
{

    public function boot()
    {
        $this->registerVoyagerMenu();
        app(Dispatcher::class)->listen('voyager.admin.routing', function ($router) {
            $this->addAlbumsRoutes($router);
        });
    }

    public function register()
    {
    	if(request()->is(config('voyager.prefix')) || request()->is(config('voyager.prefix').'/*')){
            $this->addAlbumsTable();
            $this->registerVoyagerMenu();
        }

        $this->loadViewsFrom(__DIR__ . '/../resources/views/albums', 'Albums');
    }

    public function addAlbumsRoutes($router)
    {
        $namespacePrefix = 'App\\Http\\Controllers\\';

        // Image uploads
        $router->post('albums/upload', ['uses' => $namespacePrefix.'PhotosController@upload',     'as' => 'voyager.albums.image.upload']);

        // Albums
        $router->get('albums', ['uses' => $namespacePrefix.'AlbumsController@index',   'as' => 'voyager.albums.index']);
        $router->get('albums/create', ['uses' => $namespacePrefix.'AlbumsController@create',     'as' => 'voyager.albums.create']);
        $router->post('albums', ['uses' => $namespacePrefix.'AlbumsController@store',     'as' => 'voyager.albums.store']);
        $router->get('albums/{id}/activate', ['uses' => $namespacePrefix.'AlbumsController@activate_album',     'as' => 'voyager.albums.activate']);
        $router->get('albums/{id}/desactivate', ['uses' => $namespacePrefix.'AlbumsController@desactivate_album',     'as' => 'voyager.albums.desactivate']);
        $router->get('albums/{id}/edit', ['uses' => $namespacePrefix.'AlbumsController@edit', 'as' => 'voyager.albums.edit']);
        $router->post('albums/{id}/edit', ['uses' => $namespacePrefix.'AlbumsController@update', 'as' => 'voyager.albums.update']);
        $router->delete('albums/delete', ['uses' => $namespacePrefix.'AlbumsController@destroy', 'as' => 'voyager.albums.delete']);

        // Photos
        $router->get('albums/{id}', ['uses' => $namespacePrefix.'PhotosController@index',   'as' => 'voyager.photos.index']);
        $router->post('albums/{id}', ['uses' => $namespacePrefix.'PhotosController@store',   'as' => 'voyager.photos.store']);
        $router->get('albums/{id?}/create', ['uses' => $namespacePrefix.'PhotosController@create', 'as' => 'voyager.photos.create']);
        $router->get('albums/{id}/{image_id}/edit', ['uses' => $namespacePrefix.'PhotosController@edit', 'as' => 'voyager.photos.edit']);
        $router->post('albums/{id}/{image_id}/edit', ['uses' => $namespacePrefix.'PhotosController@update', 'as' => 'voyager.photos.update']);
        $router->delete('albums/{aid}/image_delete', ['uses' => $namespacePrefix.'PhotosController@destroy', 'as' => 'voyager.photos.delete']);

        // Options
        $router->get('albums/options', function(){ return redirect( route('voyager.albums.index') ); });
    }

    public function addAlbumsMenuItem(Menu $menu)
    {

        if ($menu->name == 'admin') {
            $url = route('voyager.albums.index', [], false);
            $menuItem = $menu->items->where('url', $url)->first();
            if (is_null($menuItem)) {
                $menu->items->add(MenuItem::create([
                    'menu_id'    => $menu->id,
                    'url'        => $url,
                    'title'      => 'Albums',
                    'target'     => '_self',
                    'icon_class' => 'voyager-camera',
                    'color'      => '#7b69db',
                    'parent_id'  => null,
                    'order'      => 98,
                ]));

                $this->ensurePermissionExist();
                return redirect()->back();
            }
        }
    }

    protected function ensurePermissionExist()
    {
        $table_name = "albums";
        $table_name2 = "photos";

        $permission[0] = Permission::firstOrNew(['key' => 'browse_'.$table_name, 'table_name' => $table_name]);
        $permission[1] = Permission::firstOrNew(['key' => 'read_'.$table_name, 'table_name' => $table_name]);
        $permission[2] = Permission::firstOrNew(['key' => 'edit_'.$table_name, 'table_name' => $table_name]);
        $permission[3] = Permission::firstOrNew(['key' => 'add_'.$table_name, 'table_name' => $table_name]);
        $permission[4] = Permission::firstOrNew(['key' => 'delete_'.$table_name, 'table_name' => $table_name]);

        $permission2[0] = Permission::firstOrNew(['key' => 'browse_'.$table_name2, 'table_name' => $table_name2]);
        $permission2[1] = Permission::firstOrNew(['key' => 'read_'.$table_name2, 'table_name' => $table_name2]);
        $permission2[2] = Permission::firstOrNew(['key' => 'edit_'.$table_name2, 'table_name' => $table_name2]);
        $permission2[3] = Permission::firstOrNew(['key' => 'add_'.$table_name2, 'table_name' => $table_name2]);
        $permission2[4] = Permission::firstOrNew(['key' => 'delete_'.$table_name2, 'table_name' => $table_name2]);

        foreach ($permission as $key => $value ){
            if (!$permission[$key]->exists) {
                $permission[$key]->save();
                $role = Role::where('name', 'admin')->first();
                if (!is_null($role)) {
                    $role->permissions()->attach($permission[$key]);
                }
            }
        }

        foreach ($permission2 as $key => $value ){
            if (!$permission2[$key]->exists) {
                $permission2[$key]->save();
                $role = Role::where('name', 'admin')->first();
                if (!is_null($role)) {
                    $role->permissions()->attach($permission2[$key]);
                }
            }
        }
    }

    private function addAlbumsTable(){

        if(Schema::hasTable('albums') && !Schema::hasColumn('albums', 'slug')){

        	/*

        	 Schema::table('albums', function (Blueprint $table) {
                $table->string('slug')->unique()->default(1);
            	$table->tinyInteger('active')->default(1);
            });

			$existing_albums = Album::where('slug', '')->get();

			foreach($existing_albums as $album) {
				$album->slug = str_random(10);
				$album->save();
			}

            Schema::table('photos', function (Blueprint $table) {
                $table->string('thumbnail');
            	$table->integer('sort')->nullable();
            	$table->renameColumn('url', 'image');
            });

            */

            /*
            $migration = DB::table('migrations')->max('batch');
            DB::table('migrations')->insert(
                ['migration' => "2020_12_14_085037_albums_update_for_multiple_photo_uploads", 'batch' => $migration]
            );
            DB::table('migrations')->insert(
                ['migration' => "2020_12_14_090401_photos_update_for_multiple_photo_uploads", 'batch' => $migration]
            );
            */
        }
    }

    private function registerVoyagerMenu()
    {
        app(Dispatcher::class)->listen('voyager.menu.display', function ($menu) {
            $this->addAlbumsMenuItem($menu);
        });
    }
}
