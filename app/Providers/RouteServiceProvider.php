<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Support\Facades\RateLimiter;

class RouteServiceProvider extends ServiceProvider
{
	/**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * If specified, this namespace is automatically applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
    	$this->configureRateLimiting();

    	// Important : IDs in route pattern are constrained to be numeric
        Route::pattern('id', '[0-9]+');

        $this->routes(function () {
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

            Route::prefix('data')
                ->middleware('data')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
		    return Limit::perMinute(1000);
		});

		RateLimiter::for('auth', function (Request $request) {
		    return [
		        Limit::perMinute(20),
		        Limit::perMinute(5)->by($request->input('email')),
		    ];
		});

		RateLimiter::for('userpost', function (Request $request) {
		    return [
		        Limit::perDay(30),
		        Limit::perMonth(3)->by($request->ip()),
		    ];
		});
    }
}
