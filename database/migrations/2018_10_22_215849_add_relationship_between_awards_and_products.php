<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipBetweenAwardsAndProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    	if(Schema::hasTable('products') && Schema::hasTable('news_items')) {

    		// Add reference to product in the news_items table
			Schema::table('news_items', function (Blueprint $table) {
			    $table->integer('product_id')->unsigned()->nullable()->after('year');
			    $table->foreign('product_id')->references('id')->on('products')->onUpdate('cascade')->onDelete('cascade');
			});
    	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('news_items')) {

    		// Remove the reference to products in the news_items table
			Schema::table('news_items', function (Blueprint $table) {
				$table->dropForeign(['product_id']);
				$table->dropColumn('product_id');
			});
    	}
    }
}
