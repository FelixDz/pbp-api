<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRetailOutletsTable extends Migration
{

    public function up()
    {
        Schema::create('retail_outlets', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name', 70);
            $table->string('category', 50);
            $table->text('comment')->nullable();
            $table->string('address', 150);
            $table->integer('zipcode')->unsigned()->nullable();
            $table->string('city', 50);
            $table->string('phone', 20)->nullable();
            $table->string('web', 150)->nullable();
            // Constraints declaration
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('retail_outlets');
    }
}
