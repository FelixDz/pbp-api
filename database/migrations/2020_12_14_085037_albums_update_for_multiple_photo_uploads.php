<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

use App\Models\Album;

class AlbumsUpdateForMultiplePhotoUploads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->string('slug')->unique()->after('title');
            $table->tinyInteger('active')->default(1);
        });

        // Get existing albums and generate their slugs
        $existing_albums = Album::where('slug', '')->get();
		foreach($existing_albums as $album) {
			$album->slug = $this->generateSlug($album->slug);
			$album->save();
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('albums', function (Blueprint $table) {
            $table->dropColumn('slug');
            $table->dropColumn('active');
        });
    }

    // Generate slugs
    public function generateSlug($title){
        return Str::slug($title, '-');
    }
}
