<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRelationshipBetweenPhotosAndAlbums extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('photos', function (Blueprint $table) {
        	$table->integer('album_id')->unsigned()->change();
			$table->foreign('album_id')->references('id')->on('albums')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    	// Remove the reference to albums in the photos table
        Schema::table('photos', function (Blueprint $table) {
			$table->dropForeign(['album_id']);
			$table->integer('album_id')->change();
        });
    }
}
