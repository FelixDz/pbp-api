<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatingProductsSpecsBitternessBody extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('ibu');
            $table->unsignedDecimal('body', 2, 1)->after('abv')->nullable();
            $table->unsignedDecimal('bitterness', 2, 1)->after('body')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('body');
            $table->dropColumn('bitterness');
        	$table->integer('ibu')->unsigned()->nullable();
        });
    }
}
