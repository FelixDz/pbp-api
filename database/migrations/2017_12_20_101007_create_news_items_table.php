<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsItemsTable extends Migration
{

    public function up()
    {
        Schema::create('news_items', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title', 50);
            $table->string('category', 50);
            $table->string('image')->nullable();
            $table->text('desc');
            $table->date('post_date');
            $table->date('starting_date')->nullable();
            $table->date('ending_date')->nullable();
            $table->string('hours')->nullable();
            $table->string('place')->nullable();
            $table->integer('year')->nullable();
            // Constraints declaration
            $table->timestamps();
            $table->softDeletes();
        });
    }

    public function down()
    {
        Schema::dropIfExists('news_items');
    }
}
