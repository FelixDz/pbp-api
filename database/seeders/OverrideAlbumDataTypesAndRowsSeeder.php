<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class OverrideAlbumDataTypesAndRowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Custom albums
        $dataType = $this->dataType('slug', 'albums');
        $dataType->fill([
            'name'                  => 'albums',
            'slug'					=> 'albums',
            'display_name_singular' => 'Album',
            'display_name_plural'   => 'Albums',
            'icon'                  => 'voyager-camera',
            'model_name'            => 'App\Models\Album',
            'controller'            => 'App\Http\Controllers\AlbumsController',
            'generate_permissions'  => 1,
            'description'           => 'Albums photos de la brasserie',
            'server_side' 			=> false,
            'order_column' 			=> 'order',
            'order_direction' 		=> 'asc',
            'order_display_column' 	=> 'title',
        ])->save();

        // ████████████████████████████████████████████████
        // ████████████████████████████████████████████████
        
        $albumDataType = DataType::where('slug', 'albums')->firstOrFail();

        // Albums

        $dataRow = $this->dataRow($albumDataType, 'id');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'ID',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 0,
            'add'          => 0,
            'delete'       => 0,
            'order'        => 1,
            'details'      => null,
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'title');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Titre',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 1,
            'details'      => [
                'default' => 'Titre de l\'album photo',
                'validation' => [
                    'rule'     => 'required',
                    'messages' => [
                        'required' => 'Ce champ est requis.',
                    ],
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'slug');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'slug',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 2,
            'details'      => [
                'slugify' => [
                    'origin' => 'title',
                ],
                'validation' => [
                    'rule'     => 'required|unique:albums,slug',
                    'messages' => [
                        'required' => 'Ce champ est requis.',
                        'unique' => 'Le "slug" doit être unique et ne pas déjà exister pour un autre album.',
                    ],
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'cover_image');
        $dataRow->fill([
            'type'         => 'image',
            'display_name' => 'Image de couverture',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 3,
            'details'      => [
                'resize' => [
                    'width'  => "300",
                    'height' => null,
                ],
                'quality' => "95%",
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'category');
        $dataRow->fill([
            'type'         => 'select_dropdown',
            'display_name' => 'Catégorie',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 4,
            'details'      => [
                'default' => 'event_home',
                'options' => [
                    'event_home' => "Évènement (à la brasserie)",
                    'event'      => "Évènement (ailleurs)",
                    'brewery'    => "Photos de la brasserie",
                    'other'      => "Autre",
                ],
            ],
        ])->save();
        
        $dataRow = $this->dataRow($albumDataType, 'date');
        $dataRow->fill([
            'type'         => 'date',
            'display_name' => 'Date',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 5,
            'details'      => [
                'format' => '%d/%m/%Y',
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'desc');
        $dataRow->fill([
            'type'         => 'text_area',
            'display_name' => 'Description de l\'album',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 6,
            'details'      => [
                'default' => 'Ce texte sera affiché comme en-tête de l\'album, au-dessus des photos.',
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'order');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'Ordre',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 7,
            'details'      => [
                'validation' => [
                    'rule'     => 'required|unique',
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($albumDataType, 'active');
        $dataRow->fill([
            'type'         => 'checkbox',
            'display_name' => 'Statut',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 8,
            'details'      => [
                'on' => 'Activé',
                'off' => 'Désactivé',
                'checked' => true,
            ],
        ])->save();
    }

    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
