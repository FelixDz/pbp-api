<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

		// Normal users
    	/*
    	User::factory()
    		->times(5)
    		->create();
    	*/

        // Creating the reviewer user
    	User::factory()
    		->times(1)
    		->reviewerUser()
    		->create();

        // And the admin
    	User::factory()
    		->times(1)
    		->adminUser()
    		->create();
    }
}
