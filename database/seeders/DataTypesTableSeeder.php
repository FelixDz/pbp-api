<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;

class DataTypesTableSeeder extends Seeder
{
    /**
     * Auto generated seed file.
     */
    public function run()
    {
        $dataType = $this->dataType('slug', 'users');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'users',
                'display_name_singular' => __('voyager::seeders.data_types.user.singular'),
                'display_name_plural'   => __('voyager::seeders.data_types.user.plural'),
                'icon'                  => 'voyager-person',
                'model_name'            => 'TCG\\Voyager\\Models\\User',
                'policy_name'           => 'TCG\\Voyager\\Policies\\UserPolicy',
                'controller'            => 'TCG\\Voyager\\Http\\Controllers\\VoyagerUserController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'menus');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'menus',
                'display_name_singular' => __('voyager::seeders.data_types.menu.singular'),
                'display_name_plural'   => __('voyager::seeders.data_types.menu.plural'),
                'icon'                  => 'voyager-list',
                'model_name'            => 'TCG\\Voyager\\Models\\Menu',
                'controller'            => '',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        $dataType = $this->dataType('slug', 'roles');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'roles',
                'display_name_singular' => __('voyager::seeders.data_types.role.singular'),
                'display_name_plural'   => __('voyager::seeders.data_types.role.plural'),
                'icon'                  => 'voyager-lock',
                'model_name'            => 'TCG\\Voyager\\Models\\Role',
                'controller'            => 'TCG\\Voyager\\Http\\Controllers\\VoyagerRoleController',
                'generate_permissions'  => 1,
                'description'           => '',
            ])->save();
        }

        // Custom albums
        $dataType = $this->dataType('slug', 'albums');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'albums',
                'slug'					=> 'albums',
                'display_name_singular' => 'Album',
                'display_name_plural'   => 'Albums',
                'icon'                  => 'voyager-camera',
                'model_name'            => 'App\Models\Album',
                'controller'            => 'App\Http\Controllers\AlbumsController',
                'generate_permissions'  => 1,
                'description'           => 'Albums photos de la brasserie',
                'server_side' 			=> false,
                'order_column' 			=> 'order',
                'order_direction' 		=> 'asc',
                'order_display_column' 	=> 'title',
            ])->save();
        }

        // Custom photos
        $dataType = $this->dataType('slug', 'photos');
        if (!$dataType->exists) {
            $dataType->fill([
                'name'                  => 'photos',
                'slug'					=> 'photos',
                'display_name_singular' => 'Photo',
                'display_name_plural'   => 'Photos',
                'icon'                  => 'voyager-images',
                'model_name'            => 'App\Models\Photo',
                'controller'            => 'App\Http\Controllers\PhotosController',
                'generate_permissions'  => 1,
                'description'           => 'Photos',
                'server_side' 			=> false,
                'order_column' 			=> 'sort',
                'order_direction' 		=> 'asc',
                'order_display_column' 	=> 'image',
            ])->save();
        }
    }

    /**
     * [dataType description].
     *
     * @param [type] $field [description]
     * @param [type] $for   [description]
     *
     * @return [type] [description]
     */
    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }
}
