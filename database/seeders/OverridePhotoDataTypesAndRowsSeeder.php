<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use TCG\Voyager\Models\DataType;
use TCG\Voyager\Models\DataRow;

class OverridePhotoDataTypesAndRowsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Custom photos
        $dataType = $this->dataType('slug', 'photos');
        $dataType->fill([
            'name'                  => 'photos',
            'slug'					=> 'photos',
            'display_name_singular' => 'Photo',
            'display_name_plural'   => 'Photos',
            'icon'                  => 'voyager-images',
            'model_name'            => 'App\Models\Photo',
            'controller'            => 'App\Http\Controllers\PhotosController',
            'generate_permissions'  => 1,
            'description'           => 'Photos',
            'server_side' 			=> false,
            'order_column' 			=> 'sort',
            'order_direction' 		=> 'asc',
            'order_display_column' 	=> 'image',
        ])->save();

        // ████████████████████████████████████████████████
        // ████████████████████████████████████████████████
        
        $photoDataType = DataType::where('slug', 'photos')->firstOrFail();

        // Photos

        $dataRow = $this->dataRow($photoDataType, 'id');
        $dataRow->fill([
            'type'         => 'hidden',
            'display_name' => 'ID',
            'required'     => 1,
            'browse'       => 0,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 1,
            'details'      => null,
        ])->save();

        $dataRow = $this->dataRow($photoDataType, 'album_id');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'ID de l\'album',
            'required'     => 1,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 1,
            'details'      => null,
        ])->save();

        $dataRow = $this->dataRow($photoDataType, 'image');
        $dataRow->fill([
            'type'         => 'multiple_images',
            'display_name' => 'Image',
            'required'     => 0,
            'browse'       => 0,
            'read'         => 0,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 2,
            'details'      => [
                'resize' => [
                    'width'  => '1200',
                    'height' => null,
                ],
                'quality' => '85%',
                'thumbnails' => [
                	[
                        'name'  => 'small',
                        'scale' => '30%',
                    ],
                    [
                        'name'  => 'thumb',
                        'crop' => [
                        	'width'  => 150,
                        	'height' => 150,
                        ],
                    ],
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($photoDataType, 'legend');
        $dataRow->fill([
            'type'         => 'text',
            'display_name' => 'Légende',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 1,
            'order'        => 3,
            'details'      => [
                'validation' => [
                    'rule'     => 'max:150',
                    'messages' => [
                    	'max' => 'Maximum 150 caractères.',
                    ],
                ],
            ],
        ])->save();

        $dataRow = $this->dataRow($photoDataType, 'sort');
        $dataRow->fill([
            'type'         => 'number',
            'display_name' => 'Ordre d\'affichage',
            'required'     => 0,
            'browse'       => 1,
            'read'         => 1,
            'edit'         => 1,
            'add'          => 1,
            'delete'       => 0,
            'order'        => 4,
            'details'      => null,
        ])->save();
    }

    protected function dataType($field, $for)
    {
        return DataType::firstOrNew([$field => $for]);
    }

    protected function dataRow($type, $field)
    {
        return DataRow::firstOrNew([
            'data_type_id' => $type->id,
            'field'        => $field,
        ]);
    }
}
