<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            VoyagerDatabaseSeeder::class,
            AlbumsTableSeeder::class,
            PhotosTableSeeder::class,
            ProductsTableSeeder::class,
            NewsItemsTableSeeder::class,
            RetailOutletsTableSeeder::class,
            ReviewsTableSeeder::class,
        ]);
    }
}
