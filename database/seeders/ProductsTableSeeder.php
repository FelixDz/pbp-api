<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Product;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Creating the "general" product for global rewards / nominations
    	Product::factory()
    		->times(1)
    		->globalProduct()
    		->create();

        // Creating a bunch of beers
    	Product::factory()
    		->times(6)
    		->beerProduct()
    		->create();

        // And other products
    	Product::factory()
    		->times(2)
    		->create();
    }
}
