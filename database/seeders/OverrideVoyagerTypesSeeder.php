<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class OverrideVoyagerTypesSeeder extends Seeder
{
    /**
     * Seed all latest Voyager DataTypes and DataRows overrides
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            OverrideAlbumDataTypesAndRowsSeeder::class,
            OverridePhotoDataTypesAndRowsSeeder::class,
        ]);
    }
}
