<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\RetailOutlet;

class RetailOutletsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	RetailOutlet::factory()
    		->times(30)
    		->create();
    }
}
