<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\NewsItem;

class NewsItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	// Creating some news items (general or about products)
    	NewsItem::factory()
    		->times(3)
    		->create();

        // Some events
    	NewsItem::factory()
    		->times(5)
    		->eventNews()
    		->create();
    	NewsItem::factory()
    		->times(1)
    		->eventHomeNews()
    		->create();

        // And awards
    	NewsItem::factory()
    		->times(2)
    		->awardGlobalNews()
    		->create();
    	NewsItem::factory()
    		->times(4)
    		->awardNews()
    		->create();
    }
}
