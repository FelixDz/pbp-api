<?php

namespace Database\Factories;

use App\Models\Review;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\Review.
 */

class ReviewFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Review::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	return [
	        'author' => $this->faker->name,
	        'category' => $this->faker->randomElement(['general', 'beers', 'biergarten', 'visit', 'event']),
	        'image' => $this->faker->imageUrl(150, 150, 'people'),
	        'desc' => $this->faker->text,
	        'via' => $this->faker->randomElement(['Facebook', 'TripAdvisor', 'Google+', null]),
	        'rating' => $this->faker->randomElement([$this->faker->randomFloat(1, 3.2, 5.0), null]),
	    ];
    }
}
