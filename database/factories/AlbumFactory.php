<?php

namespace Database\Factories;

use App\Models\Album;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\Album.
 */

class AlbumFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Album::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	$date = $this->faker->dateTimeBetween('- 100 days', 'now', 'Europe/Paris');
    	$title = $this->faker->unique()->sentence(4, true);

	    return [
	        'title' => $title,
	        'slug' => Str::slug($title, '-'),
	        'category' => $this->faker->randomElement(['event_home', 'event', 'brewery']),
	        'date' => $date,
	        'desc' => $this->faker->randomElement([null, $this->faker->text]),
	        'order' => $this->faker->unique()->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]),
	        'cover_image' => $this->faker->randomElement([null, $this->faker->imageUrl(800, 600)]),
	        'active' => 1,
	    ];
    }
}
