<?php

namespace Database\Factories;

use App\Models\Photo;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\Photo.
 */

class PhotoFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Photo::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
	    return [
	        'album_id' => $this->faker->randomElement([1, 2, 3, 4, 5, 6, 7]),
	        'image' => $this->faker->imageUrl(1300, 900),
	        'legend' => $this->faker->randomElement([null, $this->faker->sentence()]),
	        'sort' => null,
	    ];
    }
}
