<?php

use Faker\Generator as Faker;

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\User.
 */

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	$hasher = app()->make('hash');

	    return [
	        'name' => $this->faker->name,
	        'email' => $this->faker->unique()->safeEmail,
	        'email_verified_at' => now(),
	        'password' => $hasher->make("secret"),
	        'remember_token' => str_random(10),
	    ];
    }

    // Reviewer
    public function reviewerUser()
	{
		return $this->state(function (array $attributes) {

			$hasher = app()->make('hash');

	        return [
	            'name' => "Pascal",
		        'email' => "pepindefresnoy@gmail.com",
		        'email_verified_at' => now(),
		        'password' => $hasher->make("zobzobzob"),
		        'remember_token' => str_random(10),
	        ];
	    });
	}

	// Admin
    public function adminUser()
	{
	    return $this->state(function (array $attributes) {

			$hasher = app()->make('hash');

	        return [
	            'name' => "Félix",
		        'email' => "felix@zaclys.net",
		        'email_verified_at' => now(),
		        'password' => $hasher->make("zobzobzob"),
		        'remember_token' => str_random(10),
	        ];
	    });
	}
}
