<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\Product.
 */

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
	    return [
	        'name' => $this->faker->word,
	        'category' => 'other',
	        'image' => $this->faker->imageUrl(500, 500),
	        'desc' => $this->faker->text,
	    ];
    }

    // Beer
    public function beerProduct()
	{
	    return $this->state(function (array $attributes) {
	        return [
	            'category' => 'beer',
		        'type' => $this->faker->randomElement(['Blonde', 'Ambrée', 'Porter', 'Blanche', 'IPA', 'Noël', 'Brune']),
		        'abv' => $this->faker->randomFloat(1, 4.4, 13.5),
		        'body' => $this->faker->numberBetween(1, 5),
		        'bitterness' => $this->faker->numberBetween(1, 5),
	        ];
	    });
	}

	// Global
    public function globalProduct()
	{
	    return $this->state(function (array $attributes) {
	        return [
	            'category' => 'global',
	        ];
	    });
	}
}
