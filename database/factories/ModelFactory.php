<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/*
| To check all available Faker data models, see : https://github.com/fzaninotto/Faker#fakerproviderbase
*/

/*
namespace Database\Factories;

use App\Model;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;


class ModelFactory extends Factory
{

    protected $model = Model::class;

    public function definition()
    {
	    return [
	        'title' => $this->faker->sentence(4, true),
	        'category' => $this->faker->randomElement(['event_home', 'event', 'brewery']),
	        'date' => $date,
	        'desc' => $this->faker->randomElement([null, $this->faker->text]),
	        'order' => $this->faker->unique()->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]),
	        'cover_image' => $this->faker->randomElement([null, $this->faker->imageUrl(800, 600)]),
	    ];
    }
*/
