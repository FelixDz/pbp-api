<?php

namespace Database\Factories;

use App\Models\NewsItem;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use DateTime;
use DatePeriod;
use DateInterval;

/**
 * Factory definition for model App\Models\NewsItem.
 */

class NewsItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NewsItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	$date = $this->faker->dateTimeThisMonth('now', 'Europe/Paris');

	    return [
	        'title' => $this->faker->sentence(3, true),
	        'category' => $this->faker->randomElement(['general', 'product']),
	        'image' => null,
	        'desc' => $this->faker->paragraphs($nb = 4, $asText = true),
	        'post_date' => $date,
	        'starting_date' => $date,
	    ];
    }

    // Event
    public function eventNews()
	{
	    return $this->state(function (array $attributes) {

	    	$start_date = $this->faker->dateTimeBetween('- 20 days', '+ 40 days', 'Europe/Paris');

	        return [
	            'category' => 'event',
		        'image' => $this->faker->imageUrl(800, 300, 'city'),
		        'starting_date' => $start_date,
		        'ending_date' => $start_date->add(new DateInterval('P1D')),	// Add a day
		        'hours' => 'Samedi : 8h - 17h | Dimanche : 9h - 12h',
		        'place' => $this->faker->city,
	        ];
	    });
	}

	// Event at the brewery
    public function eventHomeNews()
	{
	    return $this->state(function (array $attributes) {

	    	$start_date = $this->faker->dateTimeBetween('now', '+ 10 days', 'Europe/Paris');

	        return [
	            'category' => 'event_home',
		        'image' => $this->faker->imageUrl(800, 300, 'city'),
		        'starting_date' => $start_date,
		        'ending_date' => $start_date,
		        'hours' => '20h30',
		        'place' => 'la brasserie',
	        ];
	    });
	}

	// Global award
    public function awardGlobalNews()
	{
	    return $this->state(function (array $attributes) {

	        return [
	            'category' => 'award',
		        'image' => $this->faker->imageUrl(200, 200),
		        'year' => $this->faker->numberBetween(2012, 2017),
		        'award_title' => $this->faker->randomElement(['Gault & Millau', 'Petit Futé', 'Guide du Routard']),
		        'product_id' => 1,
	        ];
	    });
	}

	// Beer award
    public function awardNews()
	{
	    return $this->state(function (array $attributes) {

	        return [
	            'category' => 'award',
		        'image' => $this->faker->imageUrl(200, 200),
		        'year' => $this->faker->numberBetween(2012, 2017),
		        'award_title' => $this->faker->randomElement(['Dublin Beer Cup', 'Saint-Nicolas-de-Port', 'La Capelle', 'Guide Hachette des bières']),
		        'product_id' => $this->faker->numberBetween(2, 5),
	        ];
	    });
	}
}
