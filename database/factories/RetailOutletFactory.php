<?php

namespace Database\Factories;

use App\Models\RetailOutlet;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * Factory definition for model App\Models\RetailOutlet.
 */

class RetailOutletFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RetailOutlet::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
    	return [
	        'name' => $this->faker->company,
	        'category' => $this->faker->randomElement(['Dépôt', 'Restaurant']),
	        'comment' => $this->faker->text,
	        'address' => $this->faker->streetAddress,
	        'zipcode' => $this->faker->numerify('#####'),
	        'city' => $this->faker->city,
	        'phone' => $this->faker->e164PhoneNumber,
	        'web' => $this->faker->url,
	    ];
    }
}
