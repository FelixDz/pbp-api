<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
// This is a dev route for easily generating a 32 characters long random key for the app.
Route::get('/key', function () {
    return str_random(32);
});
*/

// Auth routes with rate limiting
Route::middleware(['throttle:login'])->group(function () {
	Auth::routes();
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();

    Route::group(['as' => 'voyager.'], function () {

	    // Image uploads
	    Route::post('albums/upload', ['uses' => 'PhotosController@upload', 'as' => 'albums.image.upload']);

	    // Albums
	    Route::get('albums', ['uses' => 'AlbumsController@index', 'as' => 'albums.index']);
	    Route::get('albums/create', ['uses' => 'AlbumsController@create', 'as' => 'albums.create']);
	    Route::post('albums', ['uses' => 'AlbumsController@store', 'as' => 'albums.store']);
	    Route::get('albums/order', ['uses' => 'AlbumsController@order', 'as' => 'albums.order']);
	    Route::post('albums/order', ['uses' => 'AlbumsController@update_order', 'as' => 'albums.order']);
	    Route::get('albums/{id}/activate', ['uses' => 'AlbumsController@activate_album', 'as' => 'albums.activate']);
	    Route::get('albums/{id}/desactivate', ['uses' => 'AlbumsController@desactivate_album', 'as' => 'albums.desactivate']);
	    Route::get('albums/{id}/edit', ['uses' => 'AlbumsController@edit', 'as' => 'albums.edit']);
	    Route::put('albums/{id}/edit', ['uses' => 'AlbumsController@update', 'as' => 'albums.update']);
	    Route::delete('albums/delete', ['uses' => 'AlbumsController@destroy', 'as' => 'albums.delete']);

	    // Photos
	    Route::get('photos', ['uses' => 'AlbumsController@index', 'as' => 'photos.index']);
	    Route::get('photos/{id?}', ['uses' => 'PhotosController@show', 'as' => 'photos.index']);
	    Route::post('photos/{id}', ['uses' => 'PhotosController@store', 'as' => 'photos.store']);
	    Route::get('photos/{id?}/create', ['uses' => 'PhotosController@create', 'as' => 'photos.create']);
	    Route::get('photos/{id}/order', ['uses' => 'PhotosController@order', 'as' => 'photos.order']);
	    Route::post('photos/{id}/order', ['uses' => 'PhotosController@update_order', 'as' => 'photos.order']);
	    Route::get('photos/{id}/{image_id}/edit', ['uses' => 'PhotosController@edit', 'as' => 'photos.edit']);
	    Route::put('photos/{id}/{image_id}/edit', ['uses' => 'PhotosController@update', 'as' => 'photos.update']);
	    Route::delete('photos/{aid}/image_delete', ['uses' => 'PhotosController@destroy', 'as' => 'photos.delete']);

	    // Options
	    Route::get('albums/options', function(){ return redirect( route('voyager.albums.index') ); });
	});
});
