<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// The "data" prefix is not needed as it is applied automatically within this specific routing file.

Route::middleware(['data', 'throttle:api'])->group(function () {

	// Query Builder docs : https://laravel.com/docs/8.x/queries

	// For use in case we re-use custom admin middleware :
	// Route::middleware(['auth:api', 'admin'])->group(function () {

	/**
	 * Products
	 */
	Route::get('product', 'ProductsController@all');
	Route::get('products', 'ProductsController@all');
	Route::get('product/{id}', 'ProductsController@get');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('product', 'ProductsController@add');
		Route::put('product/{id}', 'ProductsController@put');
		Route::delete('product/{id}', 'ProductsController@remove');
	});


	/**
	 * News items
	 */
	Route::get('news', 'NewsItemsController@all');
	Route::get('news/latest', 'NewsItemsController@getLatest');
	Route::get('news/{id}', 'NewsItemsController@get');
	// Additionnal routes for specific categories
	Route::get('award', 'NewsItemsController@allAwards');
	Route::get('awards', 'NewsItemsController@allAwards');
	Route::get('award/{id}', 'NewsItemsController@getAward');
	Route::get('event', 'NewsItemsController@allEvents');
	Route::get('events', 'NewsItemsController@allEvents');
	Route::get('event/{id}', 'NewsItemsController@getEvent');
	Route::get('event-elsewhere', 'NewsItemsController@allEventsElsewhere');
	Route::get('events-elsewhere', 'NewsItemsController@allEventsElsewhere');
	Route::get('event-elsewhere/{id}', 'NewsItemsController@getEvent');
	Route::get('event-home', 'NewsItemsController@allEventsHome');
	Route::get('events-home', 'NewsItemsController@allEventsHome');
	Route::get('event-home/{id}', 'NewsItemsController@getEvent');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('news', 'NewsItemsController@add');
		Route::put('news/{id}', 'NewsItemsController@put');
		Route::delete('news/{id}', 'NewsItemsController@remove');
	});

	/**
	 * Reviews
	 */
	Route::get('review', 'ReviewsController@all');
	Route::get('reviews', 'ReviewsController@all');
	Route::get('review/{id}', 'ReviewsController@get');
	Route::get('reviews/{category}', 'ReviewsController@getCategory');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('review', 'ReviewsController@add');
		Route::put('review/{id}', 'ReviewsController@put');
		Route::delete('review/{id}', 'ReviewsController@remove');
	});

	/**
	 * Albums
	 */
	Route::get('album', 'AlbumsController@all');
	Route::get('albums', 'AlbumsController@all');
	Route::get('album/{id}', 'AlbumsController@get');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('album', 'AlbumsController@add');
		Route::put('album/{id}', 'AlbumsController@put');
		Route::delete('album/{id}', 'AlbumsController@remove');
	});

	/**
	 * Photos
	 */
	Route::get('photo/{id}', 'PhotosController@get');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('photo', 'PhotosController@add');
		Route::put('photo/{id}', 'PhotosController@put');
		Route::delete('photo/{id}', 'PhotosController@remove');
	});

	/**
	 * Retail outlets
	 */
	Route::get('retail-outlet', 'RetailOutletsController@all');
	Route::get('retail-outlets', 'RetailOutletsController@all');
	// Additionnal routes for specific categories
	Route::get('resellers', 'RetailOutletsController@allResellers');
	Route::get('restaurants', 'RetailOutletsController@allRestaurants');
	Route::get('retail-outlet/{id}', 'RetailOutletsController@get');
	Route::middleware(['auth:api'])->group(function () {
		Route::post('retail-outlet', 'RetailOutletsController@add');
		Route::put('retail-outlet/{id}', 'RetailOutletsController@put');
		Route::delete('retail-outlet/{id}', 'RetailOutletsController@remove');
	});
});
